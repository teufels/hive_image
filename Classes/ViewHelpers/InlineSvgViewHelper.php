<?php

namespace HIVE\HiveImage\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use InvalidArgumentException;

/**
 * Class InlineSvgViewHelper.
 **/
class InlineSvgViewHelper extends AbstractViewHelper
{
    const SELECT_STYLE_ATTRIBUTE_REGEX = ['/(style=\".*?\")/'];

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('filePath', 'string', 'absolute path to file', true);
        $this->registerArgument('removeStyleAttributes', 'boolean', 'remove Style Attributes', false, false);
        $this->registerArgument('additionalRemoveTagRegex', 'array', 'additional Remove Tag Regex', false, []);
    }

    /**
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string
    {
        $aArguments = $arguments;

        $absoluteFilePath = \TYPO3\CMS\Core\Core\Environment::getPublicPath() . $aArguments['filePath'];
        $removeStyleAttributes = $aArguments['removeStyleAttributes'];
        $additionalRemoveTagRegex =  $aArguments['additionalRemoveTagRegex'];

        if (!file_exists($absoluteFilePath)) {
            throw new InvalidArgumentException("file *$absoluteFilePath* does not exist on the server.");
        }
        $content = file_get_contents($absoluteFilePath);

        if ($removeStyleAttributes) {
            $regex = array_merge(self::SELECT_STYLE_ATTRIBUTE_REGEX, $additionalRemoveTagRegex);

            return preg_replace($regex, '', $content);
        }

        return  $content;
    }
}
