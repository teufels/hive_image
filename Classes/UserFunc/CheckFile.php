<?php
namespace HIVE\HiveImage\UserFunc;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class CheckFile {

    /**
     * svg Mime types
     *
     * @var array
     */
    protected $svgMimeTypes = ['image/svg+xml'];
    protected $imageMimeTypes = ['image/jpeg','image/png','image/webp'];

    /**
     * Evaluates
     *
     * @param string $displayCondition
     * @param array $record
     *
     * @return bool
     */
    public function isSVG($displayCondition, $record) {
        if(in_array($displayCondition['record']['uid_local'][0]['row']['mime_type'], $this->svgMimeTypes)) {
            return true;
        }
        return false;
    }

    /**
     * Evaluates
     *
     * @param string $displayCondition
     * @param array $record
     *
     * @return bool
     */
    public function isImage($displayCondition, $record) {
        if(in_array($displayCondition['record']['uid_local'][0]['row']['mime_type'], $this->imageMimeTypes)) {
            return true;
        }
        return false;
    }
}
