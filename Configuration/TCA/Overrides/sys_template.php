<?php
defined('TYPO3') or die('Access denied.');

$extensionKey = 'hive_image';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $extensionKey,
    'Configuration/TypoScript',
    'HIVE>Image'
);