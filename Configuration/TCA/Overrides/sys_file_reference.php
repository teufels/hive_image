<?php
$customColumns = [
    'rendering' => [
        'displayCond' => 'USER:HIVE\\HiveImage\\UserFunc\\CheckFile->isSVG',
        'exclude' => true,
        'label' => 'Rendering',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Default', 0, ''],
                ['Inline', 1, ''],
                ['File', 2, ''],
            ],
            'default' => 0,
        ]
    ],
    'processing' => [
        'displayCond' => 'USER:HIVE\\HiveImage\\UserFunc\\CheckFile->isImage',
        'exclude' => true,
        'label' => 'Processing',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Default', 0, ''],
                ['no processing', 1, ''],
            ],
            'default' => 0,
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'sys_file_reference',
    $customColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_file_reference',
    'imageoverlayPalette',
    'rendering, processing'
);
