![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__image-blue.svg)
![version](https://img.shields.io/badge/version-2.0.*-yellow.svg?style=flat-square)

HIVE > Image
==========
Provide Viewhelpers, Crop Variants and SVG (inline/file) Support

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_image`

### Requirements
- none

### How to use
- Install with composer
- (Optional) Import Static Template (before hive_thm_custom)
    - will be automatically imported - on problems deactivate this in the EXT Settings and manually import

### How to use (ViewHelper)
```
{namespace hiveImage=HIVE\HiveImage\ViewHelpers}
<f:format.raw>
    <hiveImage:inlineSvg filePath="/absolute/path/to/some_file.svg" removeStyleAttributes="1" />
</f:format.raw>
```

### Migration
- Change own overrides of Media/Rendering/Image.html to new one (like in this EXT)
- Update hive_ovr_fluidstyledcontent which before provides the SVG Viewhelper

### Changelog
- 2.0.0 Support Typo3 v12
- 1.2.2 Prepare for Typo3 v12 LTS
- 1.2.0 Processing selection for images
- 1.1.0 Automatically load static TS
- 1.0.0 intial