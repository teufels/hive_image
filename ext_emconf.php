<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_image".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
    'title' => 'HIVE>Image',
    'description' => 'Provide Viewhelpers, Crop Variants and SVG (inline/file) Support',
    'category' => 'fe',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '2.0.0',
    'constraints' =>
      array (
        'depends' =>
        array (
          'typo3' => '11.5.0-0.0.0',
        ),
        'conflicts' =>
        array (
        ),
        'suggests' =>
        array (
        ),
      ),
);

