<?php
defined('TYPO3') or die('Access denied.');

(function () {
    /**
     * Load default TS
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('@import "EXT:hive_image/Configuration/TypoScript/constants.typoscript"');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup('@import "EXT:hive_image/Configuration/TypoScript/setup.typoscript"');
})();